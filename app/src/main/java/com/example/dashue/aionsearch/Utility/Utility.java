package com.example.dashue.aionsearch.Utility;

import android.util.Log;

import java.net.URLEncoder;

public class Utility {
    static public String GetEncodedString(String rawString) {
        try {
            return URLEncoder.encode(rawString, "UTF-8");
        }
        catch (Exception e) {
            Log.d("ERROR", e.getMessage());
            return null;
        }
    }
}
