package com.example.dashue.aionsearch.Network;

import com.example.dashue.aionsearch.Model.Channel;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

public interface RestRequest {
    @GET("/openapi/searchmobile.jsp")
    Call<Channel> searchCharacter(@Query(value = "index", encoded = true) String index,
                                        @Query(value = "site", encoded = true) String site,
                                        @Query(value = "pos", encoded = true) String pos,
                                        @Query(value = "query", encoded = true) String query,
                                        @Query(value = "user", encoded = true) String user);
}
