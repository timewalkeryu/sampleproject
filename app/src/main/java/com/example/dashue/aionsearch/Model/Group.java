package com.example.dashue.aionsearch.Model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "group")
public class Group {
    @Element public String collection;
    @Element public int totalcount;
    @Element public int searchcount;
    @Element public int start;
    @Element public int page;

    @ElementList(inline = true)
    public List<Item> item;
}
