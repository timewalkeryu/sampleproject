package com.example.dashue.aionsearch.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.dashue.aionsearch.Model.Channel;
import com.example.dashue.aionsearch.R;
import com.example.dashue.aionsearch.Network.RestRequest;
import com.example.dashue.aionsearch.Network.ServiceGenerator;
import com.example.dashue.aionsearch.Utility.Utility;

import java.util.ArrayList;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

public class MainActivity extends AppCompatActivity {

    private ArrayAdapter _stringAdapter;
    private ArrayList<String> _searchResultList;

    private Button _searchButton;
    private ListView _searchResultListView;
    private Toolbar _mainToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        InitResources();

        BindingSearchButtonHandler();
        BindingToolbarHandler();
    }

    // content_detail.xml 각 리소스 연결
    private void InitResources() {
        _searchResultListView = (ListView)findViewById(R.id.lv_search);
        _searchButton = (Button)findViewById(R.id.bt_search);
        _mainToolbar = (Toolbar) findViewById(R.id.toolbarMain);
    }

    // 검색 버튼 눌렀을 경우 처리
    private void BindingSearchButtonHandler() {
        _searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String _index = Utility.GetEncodedString("aion^aionmobile^aionuser");
                    String _site = Utility.GetEncodedString("maion");
                    String _pos = Utility.GetEncodedString("page");
                    String _query = Utility.GetEncodedString(((EditText) findViewById(R.id.et_search)).getText().toString().trim());
                    String _user = Utility.GetEncodedString("111.111.111.11");

                    RestRequest restRequest = ServiceGenerator.createService(RestRequest.class);
                    Call<Channel> response = restRequest.searchCharacter(_index, _site, _pos, _query, _user);
                    response.enqueue(new Callback<Channel>() {
                        @Override
                        public void onResponse(Response<Channel> response) {
                            Channel _channel = response.body();

                            if (_channel == null) return;
                            if (_channel.groups.group.size() <= 0) return;
                            if (_channel.groups.group.get(0).item.size() <= 0) return;

                            _searchResultList = new ArrayList();
                            for (int i = 0, imax = _channel.groups.group.get(0).item.size(); i < imax; i++) {
                                _searchResultList.add(i, _channel.groups.group.get(0).item.get(i).char_name.replaceAll("<[^>]*>", ""));
                            }
                            UpdateListView();
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            Log.d("ERROR", t.getMessage());
                        }
                    });
                } catch (Exception e) {
                    Log.d("ERROR", e.getMessage());
                }

            }
        });
    }

    // 상단 액션바 타이틀 처리
    private void BindingToolbarHandler() {
        setSupportActionBar(_mainToolbar);
        getSupportActionBar().setTitle("케릭터 검색");
    }

    private void UpdateListView() {

        _stringAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, _searchResultList.toArray());
        _searchResultListView.setAdapter(_stringAdapter);
        _searchResultListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("TAG", String.format("%d", position));
                Intent _intent = new Intent(MainActivity.this, DetailActivity.class);
                _intent.putExtra("searchString", ((EditText) findViewById(R.id.et_search)).getText().toString().trim());
                _intent.putExtra("selectIndex", position);
                startActivity(_intent);
            }
        });
    }
}