package com.example.dashue.aionsearch.Model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "channel")
public class Channel {
    @Element public int rcode;
    @Element public String rmessage;
    @Element public Groups groups;
}