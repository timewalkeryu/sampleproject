package com.example.dashue.aionsearch.Model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "item")
public class Item {
    @Element(required = false) public String char_id;
    @Element(required = false) public String char_name;
    @Element(required = false) public String server_id;
    @Element(required = false) public String server_name;
    @Element(required = false) public String legion_id;
    @Element(required = false) public String legion_name;
    @Element(required = false) public String class_name;
    @Element(required = false) public String race_id;
    @Element(required = false) public String race_name;
    @Element(required = false) public String gender_id;
    @Element(required = false) public String abyss_rank;
    @Element(required = false) public String level;
    @Element(required = false) public String hp;
    @Element(required = false) public String mp;
    @Element(required = false) public String ap;
    @Element(required = false) public String gp;
}
