package com.example.dashue.aionsearch.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.dashue.aionsearch.Model.Channel;
import com.example.dashue.aionsearch.Model.Item;
import com.example.dashue.aionsearch.R;
import com.example.dashue.aionsearch.Network.RestRequest;
import com.example.dashue.aionsearch.Network.ServiceGenerator;
import com.example.dashue.aionsearch.Utility.Utility;

import java.net.URLEncoder;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        BindingToolbar();
        NetworkProcess();
    }

    private void NetworkProcess() {
        try {
            Intent _intent = getIntent();

            String _index = Utility.GetEncodedString("aion^aionmobile^aionuser");
            String _site = Utility.GetEncodedString("maion");
            String _pos = Utility.GetEncodedString("page");
            String _query = Utility.GetEncodedString(_intent.getStringExtra("searchString"));
            String _user = Utility.GetEncodedString("111.111.111.11");

            RestRequest restRequest = ServiceGenerator.createService(RestRequest.class);
            Call<Channel> response = restRequest.searchCharacter(_index, _site, _pos, _query, _user);
            response.enqueue(new Callback<Channel>() {
                @Override
                public void onResponse(Response<Channel> response) {
                    Log.d("DEBUG", "SUCCESS");

                    TextView _tv;

                    Intent _intent = getIntent();
                    int _index = _intent.getIntExtra("selectIndex", 0);

                    Channel _channel = response.body();
                    Item selectItem = _channel.groups.group.get(0).item.get(_index);

                    _tv = (TextView)findViewById(R.id.tv_detail_char_name);
                    _tv.setText(String.format("%s", selectItem.char_name.replaceAll("<[^>]*>", "")));

                    _tv = (TextView)findViewById(R.id.tv_detail_server_name);
                    _tv.setText(String.format("%s", selectItem.server_name));

                    _tv = (TextView)findViewById(R.id.tv_detail_legion_name);
                    _tv.setText(String.format("%s", selectItem.legion_name == null ? "" : selectItem.legion_name));

                    _tv = (TextView)findViewById(R.id.tv_detail_class_name);
                    _tv.setText(String.format("%s", selectItem.class_name));

                    _tv = (TextView)findViewById(R.id.tv_detail_race_name);
                    _tv.setText(String.format("%s", selectItem.race_name));

                    _tv = (TextView)findViewById(R.id.tv_detail_abyss_rank);
                    _tv.setText(String.format("%s", selectItem.abyss_rank));

                    _tv = (TextView)findViewById(R.id.tv_detail_level);
                    _tv.setText(String.format("%s", selectItem.level));

                    _tv = (TextView)findViewById(R.id.tv_detail_hp);
                    _tv.setText(String.format("%s", selectItem.hp));

                    _tv = (TextView)findViewById(R.id.tv_detail_mp);
                    _tv.setText(String.format("%s", selectItem.mp));
                }

                @Override
                public void onFailure(Throwable t) {
                    Log.d("ERROR", t.getMessage());
                }
            });
        }
        catch (Exception e) {
            Log.d("ERROR", e.getMessage());
        }
    }

    private void BindingToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarDetail);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("케릭터 상세");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
