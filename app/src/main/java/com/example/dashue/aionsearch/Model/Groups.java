package com.example.dashue.aionsearch.Model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "groups")
public class Groups {
    @Element public int number;

    @ElementList(inline = true)
    public List<Group> group;
}
