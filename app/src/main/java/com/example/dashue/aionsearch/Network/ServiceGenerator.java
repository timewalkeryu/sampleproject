package com.example.dashue.aionsearch.Network;

import com.squareup.okhttp.OkHttpClient;

import retrofit.Retrofit;
import retrofit.SimpleXmlConverterFactory;

public class ServiceGenerator {
    public static final  String API_BASE_URL = "http://search.plaync.com";
    private static OkHttpClient httpClient = new OkHttpClient();
    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .addConverterFactory(SimpleXmlConverterFactory.create());
    public static <S> S createService(Class<S> serviceClass) {
        Retrofit retrofit = builder.client(httpClient).build();
        return retrofit.create(serviceClass);
    }
}
